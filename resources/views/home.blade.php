<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    
    <link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />   
    

 <title>todolist</title>
</head>
<body class='bg-info  '  >
    <div class='conatainer w-25 mt-5 ms-5'>
        <div class='card shadow-sm ' >
            <div class='card-body '>
                <h3>To-do List</h3>
                <form action="{{route('store')}}" method='Post'autoComplete='off'>
                    @csrf
                    <div class='input-group'>
                        <input type="text" name='content' class='form-control' placeholder='Add your new todo'>
                        <button type='submit' class='btn btn-dark  btn-sm px- '>Add</button>

                    </div>
                </form>
                @if(count($todolists))

                <ul class='list-group list-group-float mt-3'>
                @foreach($todolists as $todolist )
                <li class=' list-group-item'>
                    <form action="{{ route('destroy' ,$todolist->id )}}" method='Post'>
                        {{$todolist->content}}
                       @csrf 
                      @method('delete')
                      <button type='submit' class='btn btn-warning btn-sm float-end'><i class="fa-solid fa-trash"></i></button>      
                    </form>

                </li>

                @endforeach

                </ul>
                @else
                <p class='text-center mt-3' >No Tasks!</p>
                @endif

            </div>
            @if(count($todolists))
            <div class='card-footer'>
             you have {{count($todolists)}} padding task

            </div>
            @endif
        

        </div>

    </div>
    
    
</body>
</html>